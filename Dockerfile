FROM alpine

RUN apk update \
  && apk add --no-cache python3 py3-paho-mqtt py3-configargparse \
  && pip3 install prometheus_client APScheduler \
  && rm -rf /var/cache/apk/*

RUN adduser -S toor
USER toor

ADD wifi-presence /usr/bin/

CMD ["python3", "/usr/bin/wifi-presence"]
